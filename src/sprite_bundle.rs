use std::marker::PhantomData;

use num_traits::Float;
use specs::System;
use specs_bundler::{Bundle, Bundler};

use super::{Sprite, SpriteSystem};

#[derive(Debug)]
pub struct SpriteBundle<'deps, T, I> {
    deps: Vec<&'deps str>,
    _marker: PhantomData<(T, I)>,
}

impl<'deps, T, I> Default for SpriteBundle<'deps, T, I> {
    #[inline]
    fn default() -> Self {
        SpriteBundle::new(&[])
    }
}

impl<'deps, T, I> SpriteBundle<'deps, T, I> {
    #[inline]
    pub fn new(deps: &[&'deps str]) -> Self {
        SpriteBundle {
            deps: deps.to_vec(),
            _marker: PhantomData,
        }
    }
}

impl<'deps, 'world, 'a, 'b, T, I> Bundle<'world, 'a, 'b> for SpriteBundle<'deps, T, I>
where
    T: 'static + Float + Send + Sync,
    I: 'static + Send + Sync,
{
    type Error = ();

    #[inline]
    fn bundle(
        self,
        mut bundler: Bundler<'world, 'a, 'b>,
    ) -> Result<Bundler<'world, 'a, 'b>, Self::Error> {
        bundler.world.register::<Sprite<T, I>>();

        let mut system = SpriteSystem::<T, I>::new();

        system.setup(&mut bundler.world.res);

        bundler.dispatcher_builder = bundler.dispatcher_builder.with(
            system,
            SpriteSystem::<T, I>::name(),
            self.deps.as_slice(),
        );

        Ok(bundler)
    }
}
