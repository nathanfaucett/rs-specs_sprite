use std::sync::atomic::{AtomicBool, Ordering};

use num_traits::Float;
use specs::{Component, DenseVecStorage, FlaggedStorage};

#[derive(Debug)]
pub struct Sprite<T, I> {
    pub image: I,
    pub x: T,
    pub y: T,
    pub w: T,
    pub h: T,
    pub width: T,
    pub height: T,
    z: usize,
    layer: usize,
    dirty: AtomicBool,
}

impl<T, I> Component for Sprite<T, I>
where
    T: 'static + Send + Sync,
    I: 'static + Send + Sync,
{
    type Storage = FlaggedStorage<Self, DenseVecStorage<Self>>;
}

impl<T, I> Sprite<T, I>
where
    T: Float,
{
    #[inline]
    pub fn new(image: I) -> Self {
        Sprite {
            image: image,
            x: T::zero(),
            y: T::zero(),
            w: T::one(),
            h: T::one(),
            width: T::one(),
            height: T::one(),
            z: 0,
            layer: 0,
            dirty: AtomicBool::new(false),
        }
    }
}

impl<T, I> Sprite<T, I> {
    #[inline]
    pub fn compare(&self, other: &Self) -> ::std::cmp::Ordering {
        if &self.layer == &other.layer {
            self.z.cmp(&other.z)
        } else {
            self.layer.cmp(&other.layer)
        }
    }

    #[inline(always)]
    pub fn z(&self) -> usize {
        self.z
    }
    #[inline(always)]
    pub fn layer(&self) -> usize {
        self.layer
    }

    #[inline]
    pub fn with_x(mut self, x: T) -> Self {
        self.x = x;
        self
    }
    #[inline]
    pub fn with_y(mut self, y: T) -> Self {
        self.y = y;
        self
    }
    #[inline]
    pub fn with_w(mut self, w: T) -> Self {
        self.w = w;
        self
    }
    #[inline]
    pub fn with_h(mut self, h: T) -> Self {
        self.h = h;
        self
    }
    #[inline]
    pub fn with_width(mut self, width: T) -> Self {
        self.width = width;
        self
    }
    #[inline]
    pub fn with_height(mut self, height: T) -> Self {
        self.height = height;
        self
    }
    #[inline]
    pub fn with_z(mut self, z: usize) -> Self {
        self.z = z;
        self
    }
    #[inline]
    pub fn with_layer(mut self, layer: usize) -> Self {
        self.layer = layer;
        self
    }
    #[inline]
    pub fn with_layer_and_z(mut self, layer: usize, z: usize) -> Self {
        self.layer = layer;
        self.z = z;
        self
    }

    #[inline]
    pub fn set_image(&mut self, image: I) -> &mut Self {
        self.image = image;
        self
    }
    #[inline]
    pub fn set_x(&mut self, x: T) -> &mut Self {
        self.x = x;
        self
    }
    #[inline]
    pub fn set_y(&mut self, y: T) -> &mut Self {
        self.y = y;
        self
    }
    #[inline]
    pub fn set_w(&mut self, w: T) -> &mut Self {
        self.w = w;
        self
    }
    #[inline]
    pub fn set_h(&mut self, h: T) -> &mut Self {
        self.h = h;
        self
    }
    #[inline]
    pub fn set_width(&mut self, width: T) -> &mut Self {
        self.width = width;
        self
    }
    #[inline]
    pub fn set_height(&mut self, height: T) -> &mut Self {
        self.height = height;
        self
    }
    #[inline]
    pub fn set_z(&mut self, z: usize) -> &mut Self {
        self.z = z;
        self.flag(true);
        self
    }
    #[inline]
    pub fn set_layer(&mut self, layer: usize) -> &mut Self {
        self.layer = layer;
        self.flag(true);
        self
    }
    #[inline]
    pub fn set_layer_and_z(&mut self, layer: usize, z: usize) -> &mut Self {
        self.layer = layer;
        self.z = z;
        self.flag(true);
        self
    }

    #[inline]
    pub(crate) fn flag(&self, dirty: bool) {
        self.dirty.store(dirty, Ordering::SeqCst)
    }
    #[inline]
    pub fn is_dirty(&self) -> bool {
        self.dirty.load(Ordering::SeqCst)
    }
}
