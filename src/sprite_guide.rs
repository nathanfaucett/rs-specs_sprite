use std::cmp::Ordering;
use std::marker::PhantomData;
use std::ops::Deref;
use std::slice;

use hibitset::BitSet;

use specs::prelude::ComponentEvent;
use specs::{Entities, Entity, Join, ReaderId, WriteStorage};

use super::Sprite;

/// if using after bundled, in systems use WriteExpect and ReadExpect
#[derive(Debug)]
pub struct SpriteGuide<T, I> {
    sorted: Vec<Entity>,

    reader_id: ReaderId<ComponentEvent>,

    modified: BitSet,
    inserted: BitSet,
    removed: BitSet,

    _marker: PhantomData<(T, I)>,
}

impl<T, I> SpriteGuide<T, I> {
    #[inline]
    pub fn new(reader_id: ReaderId<ComponentEvent>) -> Self {
        SpriteGuide {
            sorted: Vec::default(),

            reader_id: reader_id,

            modified: BitSet::default(),
            inserted: BitSet::default(),
            removed: BitSet::default(),

            _marker: PhantomData,
        }
    }

    #[inline(always)]
    pub fn as_slice(&self) -> &[Entity] {
        &*self.sorted
    }
}

impl<T, I> SpriteGuide<T, I>
where
    T: 'static + Send + Sync,
    I: 'static + Send + Sync,
{
    #[inline]
    pub fn maintain<'system>(
        &mut self,
        entities: Entities<'system>,
        sprites: WriteStorage<'system, Sprite<T, I>>,
    ) {
        self.modified.clear();
        self.inserted.clear();
        self.removed.clear();

        for event in sprites.channel().read(&mut self.reader_id) {
            match event {
                ComponentEvent::Modified(id) => {
                    self.modified.add(*id);
                }
                ComponentEvent::Inserted(id) => {
                    self.inserted.add(*id);
                }
                ComponentEvent::Removed(id) => {
                    self.removed.add(*id);
                }
            }
        }

        for (entity, _, _sprite) in (&*entities, &self.removed, &sprites).join() {
            if let Some(index) = self.sorted.iter().position(|e| &entity == e) {
                self.sorted.remove(index);
            }
        }

        for (entity, _, sprite) in (&*entities, &self.inserted, &sprites).join() {
            if let Some(index) = self.sorted.iter().position(|e| {
                if let Some(other_sprite) = sprites.get(*e) {
                    match sprite.compare(other_sprite) {
                        Ordering::Less => true,
                        _ => false,
                    }
                } else {
                    false
                }
            }) {
                self.sorted.insert(index, entity);
            } else {
                self.sorted.push(entity);
            }
        }

        for (entity, _, sprite) in (&*entities, &self.modified, &sprites).join() {
            if let Some(index) = self.sorted.iter().position(|e| &entity == e) {
                self.sorted.remove(index);
            }

            if let Some(index) = self.sorted.iter().position(|e| {
                if let Some(other_sprite) = sprites.get(*e) {
                    match sprite.compare(other_sprite) {
                        Ordering::Less => false,
                        _ => true,
                    }
                } else {
                    false
                }
            }) {
                self.sorted.insert(index, entity);
            }
        }
    }
}

impl<T, I> Deref for SpriteGuide<T, I> {
    type Target = [Entity];

    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        self.as_slice()
    }
}

impl<'a, T, I> IntoIterator for &'a SpriteGuide<T, I> {
    type Item = &'a Entity;
    type IntoIter = slice::Iter<'a, Entity>;

    fn into_iter(self) -> Self::IntoIter {
        self.as_slice().into_iter()
    }
}
