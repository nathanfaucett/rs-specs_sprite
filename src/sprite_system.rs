use std::marker::PhantomData;

use num_traits::Float;
use specs::{Entities, System, Write, WriteStorage};
use type_name;

use super::{Sprite, SpriteGuide, SpriteGuideSetupHandler};

pub struct SpriteSystem<T, I>(PhantomData<(T, I)>);

impl<T, I> Default for SpriteSystem<T, I> {
    #[inline]
    fn default() -> Self {
        SpriteSystem(PhantomData)
    }
}

impl<T, I> SpriteSystem<T, I> {
    #[inline]
    pub fn name() -> &'static str {
        type_name::get::<Self>()
    }
}

impl<T, I> SpriteSystem<T, I> {
    #[inline]
    pub fn new() -> Self {
        Self::default()
    }
}

impl<'a, T, I> System<'a> for SpriteSystem<T, I>
where
    T: 'static + Float + Send + Sync,
    I: 'static + Send + Sync,
{
    type SystemData = (
        Entities<'a>,
        Write<'a, SpriteGuide<T, I>, SpriteGuideSetupHandler<T, I>>,
        WriteStorage<'a, Sprite<T, I>>,
    );

    #[inline]
    fn run(&mut self, (entities, mut sprite_guide, sprites): Self::SystemData) {
        sprite_guide.maintain(entities, sprites);
    }
}
