use std::marker::PhantomData;

use shred::SetupHandler;
use specs::{Resources, SystemData, WriteStorage};

use super::{Sprite, SpriteGuide};

#[derive(Debug, Default)]
pub struct SpriteGuideSetupHandler<T, I>(PhantomData<(T, I)>);

impl<T, I> SetupHandler<SpriteGuide<T, I>> for SpriteGuideSetupHandler<T, I>
where
    T: 'static + Send + Sync,
    I: 'static + Send + Sync,
{
    #[inline]
    fn setup(res: &mut Resources) {
        if !res.has_value::<SpriteGuide<T, I>>() {
            let sprites = {
                let mut storage: WriteStorage<Sprite<T, I>> = SystemData::fetch(&res);
                SpriteGuide::<T, I>::new(storage.register_reader())
            };

            res.insert(sprites);
        }
    }
}
