# specs_sprite

sprite component for specs

```rust
extern crate specs_sprite;
extern crate specs_guided_join;
extern crate specs_bundler;
extern crate specs;

use specs_sprite::{Sprite, SpriteGuide, SpriteBundle};
use specs_guided_join::GuidedJoin;
use specs_bundler::SpecsBundler;
use specs::{Builder, Join, World, DispatcherBuilder};

fn main() {
    let mut world = World::new();

    let mut dispatcher = SpecsBundler::new(&mut world, DispatcherBuilder::new())
        .bundle(SpriteBundle::<f32, usize>::default()).unwrap()
        .build();

    // just using an unsigned integer for the image
    let _ = world
        .create_entity()
        .with(Sprite::<f32, usize>::new(0).with_layer_and_z(1, 1))
        .build();

    let _ = world
        .create_entity()
        .with(Sprite::<f32, usize>::new(1).with_layer_and_z(0, 0))
        .build();

    let _ = world
        .create_entity()
        .with(Sprite::<f32, usize>::new(2).with_layer_and_z(1, 0))
        .build();

    dispatcher.dispatch(&mut world.res);

    let read_sprites = world.read::<Sprite<f32, usize>>();
    let read_guide = world.read_resource::<SpriteGuide<f32>>();

    let sprites: Vec<&Sprite<f32, usize>> = (&read_sprites).join().collect();
    let ordered_sprites: Vec<&Sprite<f32, usize>> = (&read_sprites).guided_join(read_guide.as_slice()).collect();

    assert_eq!(sprites.iter().map(|s| s.image).collect::<Vec<usize>>(), [0, 1, 2].to_vec());
    assert_eq!(ordered_sprites.iter().map(|s| s.image).collect::<Vec<usize>>(), [1, 2, 0].to_vec());
}
```
